<?php

namespace Drupal\domain_menu_links;

/**
 * Module constants.
 */
class DomainMenuLinksConstants {

  /**
   * The class used on dropdown links.
   */
  const DOMAIN_MENU_LINK_CLASS = 'domain-menu-links-link';

  /**
   * The menu visibility permission.
   */
  const DOMAIN_MENU_PERMISSION = 'view toolbar domain menu';

}
