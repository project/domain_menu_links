<?php

namespace Drupal\domain_menu_links\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\domain_menu_links\DomainMenuLinksConstants;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Derivative class that provides domains menu links.
 */
class DomainMenuLinks extends DeriverBase implements ContainerDeriverInterface {

  /**
   * The entity type manager.
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id): object {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition): array {
    $links = [];
    $domains = $this->entityTypeManager->getStorage('domain')->loadMultiple();

    foreach ($domains as $domain) {
      if (!$domain->status()) {
        continue;
      }

      $links[] = [
        'title' => $domain->label(),
        'url' => $domain->getRawPath(),
        'weight' => $domain->getWeight(),
        'parent' => 'domain_menu_links.domain',
        'options' => [
          'attributes' => [
            'target' => '_blank',
            'class' => [
              DomainMenuLinksConstants::DOMAIN_MENU_LINK_CLASS,
            ],
          ],
        ],
      ] + $base_plugin_definition;
    }

    return $links;
  }

}
