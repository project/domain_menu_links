<?php

declare(strict_types=1);

namespace Drupal\Tests\domain_menu_links\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests domain_menu_links config elements.
 *
 * @group domain_menu_links
 */
class DomainMenuLinksConfigTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['domain', 'domain_menu_links'];

  /**
   * Test setup.
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installConfig('domain_menu_links');
  }

  /**
   * Dummy test method to ensure config gets installed.
   */
  public function testRun(): void {
    $this->assertTrue(TRUE);
  }

}
