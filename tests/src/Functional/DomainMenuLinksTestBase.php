<?php

declare(strict_types=1);

namespace Drupal\Tests\domain_menu_links\Functional;

use Drupal\Tests\domain\Functional\DomainTestBase;

/**
 * Base class for tests.
 *
 * @group domain_menu_links
 */
abstract class DomainMenuLinksTestBase extends DomainTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'domain',
    'domain_menu_links',
    'user',
  ];

  /**
   * Theme to enable.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Add modules for tests.
   *
   * @param array|string[] $modules
   *   Array of modules names.
   */
  protected function addModules(array $modules): void {
    self::$modules = array_push(self::$modules, $modules);
  }

}
