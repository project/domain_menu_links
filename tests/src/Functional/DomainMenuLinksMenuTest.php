<?php

declare(strict_types=1);

namespace Drupal\Tests\domain_menu_links\Functional;

use Drupal\domain\Entity\Domain;
use Drupal\domain_menu_links\DomainMenuLinksConstants;

/**
 * Tests the toolbar domain menu.
 *
 * @group domain_menu_links
 */
class DomainMenuLinksMenuTest extends DomainMenuLinksTestBase {

  /**
   * Path used for tests.
   */
  private string $testPath = 'node/';

  /**
   * Toolbar menu xpath.
   */
  private string $toolbarXpath = "//div[@class='toolbar-menu-administration']/ul[contains(@class, 'toolbar-menu')]";

  /**
   * Toolbar domain menu links xpath.
   */
  private string $domainMenuLinksXpath = "";

  /**
   * Toolbar domain menu links list.
   */
  private array $toolbarDomainMenuLinks = [];

  /**
   * Test setup.
   */
  protected function setUp(): void {
    parent::setUp();

    $this->addModules([
      'toolbar',
      'admin_toolbar',
    ]);
    $this->domainCreateTestDomains(5);
    $this->domainMenuLinksXpath = $this->toolbarXpath . "//a[contains(@class, '" . DomainMenuLinksConstants::DOMAIN_MENU_LINK_CLASS . "')]";
  }

  /**
   * Test parent menu link.
   */
  public function testDomainMenuParentLink(): void {
    $xpath = $this->toolbarXpath . "/li/a[contains(@class, 'toolbar-icon-domain-menu')]";

    // A user without permission should not see the domain menu.
    $author = $this->createTestUser();
    $this->drupalLogin($author);
    $this->drupalGet($this->testPath);
    $this->assertEmpty($this->xpath($xpath));

    // A user with permission should see the domain menu.
    $editor = $this->createTestUser([DomainMenuLinksConstants::DOMAIN_MENU_PERMISSION]);
    $this->drupalLogin($editor);
    $this->drupalGet($this->testPath);
    $this->assertNotEmpty($this->xpath($xpath));
  }

  /**
   * Test dropdown links.
   */
  public function testDomainMenuDropdownLinks(): void {
    // A user without permission should not see the domain menu links.
    $editor = $this->createTestUser();
    $this->drupalLogin($editor);
    $this->drupalGet($this->testPath);
    $this->assertEmpty($this->xpath($this->domainMenuLinksXpath));

    // A user with permission should see the domain menu links.
    $admin = $this->createTestUser([DomainMenuLinksConstants::DOMAIN_MENU_PERMISSION]);
    $this->drupalLogin($admin);
    $this->refreshToolbarDomainMenuLinks();

    // Test if links are sorted by domain weights.
    $domains_sorted = $this->getDomainsSorted();
    $domains_length = count($domains_sorted);

    foreach ($domains_sorted as $domain) {
      $this->testDomainLabelAgainstMenuLinkText($domain);

      // Invert domain weights for the next test.
      $domain->set('weight', $domains_length);
      $domain->save();
      $domains_length--;
    }

    // Test menu link order after domain updates.
    $loop_index = 0;
    $this->refreshToolbarDomainMenuLinks();

    foreach ($this->getDomainsSorted() as $domain) {
      $this->testDomainLabelAgainstMenuLinkText($domain);

      // Remove some domains for the next test.
      if ($loop_index === 2 || $loop_index === 4) {
        $domain->delete();
      }

      $loop_index++;
    }

    // Test menu links after deleting some domains.
    $this->refreshToolbarDomainMenuLinks();

    foreach ($this->getDomainsSorted() as $domain) {
      $this->testDomainLabelAgainstMenuLinkText($domain);
    }
  }

  /**
   * Creates a user for tests.
   *
   * @param array $permissions
   *   The user permissions.
   *
   * @return \Drupal\user\Entity\User|false
   *   A fully loaded user object or FALSE if account creation fails.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function createTestUser(array $permissions = []): object|bool {
    $permissions[] = 'access toolbar';
    return $this->drupalCreateUser($permissions);
  }

  /**
   * Refresh the toolbar domain menu links list.
   */
  protected function refreshToolbarDomainMenuLinks(): void {
    $this->drupalGet($this->testPath);
    $toolbar_domain_menu_links = $this->xpath($this->domainMenuLinksXpath);
    $this->assertNotEmpty($toolbar_domain_menu_links);

    $this->toolbarDomainMenuLinks = $toolbar_domain_menu_links;
  }

  /**
   * Test that the domain label matches the current menu link text.
   *
   * @param \Drupal\domain\Entity\Domain $domain
   *   The domain.
   */
  protected function testDomainLabelAgainstMenuLinkText(Domain $domain): void {
    $this->assertEquals($domain->label(), current($this->toolbarDomainMenuLinks)->getText());
    next($this->toolbarDomainMenuLinks);
  }

}
