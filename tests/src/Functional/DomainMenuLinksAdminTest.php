<?php

declare(strict_types=1);

namespace Drupal\Tests\domain_menu_links\Functional;

/**
 * Tests the configuration page.
 *
 * @group domain_menu_links
 */
class DomainMenuLinksAdminTest extends DomainMenuLinksTestBase {

  /**
   * Test access and functionality of the admin page.
   */
  public function testAdminPage(): void {
    $menu_weight = 0;
    $menu_weight_field = 'parent_menu_link_weight';
    $admin_page_path = 'admin/config/domain/domain_menu_links/settings';

    // Test a non-admin user.
    $editor = $this->drupalCreateUser(['access administration pages']);
    $this->drupalLogin($editor);
    $this->drupalGet($admin_page_path);
    $this->assertSession()->statusCodeEquals(403);

    // Test an admin user.
    $admin = $this->drupalCreateUser(['administer site configuration']);
    $this->drupalLogin($admin);
    $this->drupalGet($admin_page_path);
    $this->assertSession()->statusCodeEquals(200);
    $this->getSession()->getPage()->fillField($menu_weight_field, $menu_weight);
    $this->getSession()->getPage()->pressButton('Save configuration');

    $settings = $this->config('domain_menu_links.settings')->get($menu_weight_field);
    $this->assertEquals($menu_weight, $settings);
  }

}
