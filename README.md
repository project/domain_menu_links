INTRODUCTION
------------

Domain Menu Links is an extension of the [Domain](https://www.drupal.org/project/domain) module. Your website will have a dropdown in the admin toolbar with all registered domains. You can easily switch domains while browsing!

INSTALLATION
------------

You can follow the [drupal steps](https://www.drupal.org/docs/user_guide/en/extend-module-install.html) to download and install the module.
Check the available versions on the [module page](https://www.drupal.org/project/domain_menu_links).

HOW TO SET THE MENU LINK WEIGHT
------------
1. Go to Admin > Configuration > Domains > Domain Menu Link Settings
2. Update the **Parent menu link weight** field
3. Save configuration
